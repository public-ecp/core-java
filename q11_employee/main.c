#include <stdio.h>

void readdata();
void disdata();
typedef struct employee
{
    char fname[10];
    char lname[10];
    double salary;
    int year;
} emp;
void ACCEPT_DATA(emp *s, int count);
void PRINT_DATA(emp *s, int count);

int main()
{
    int n;
    printf("Enter the no. of employees: ");
    scanf("%d", &n);
    emp e[n];

    ACCEPT_DATA(e, n);
    PRINT_DATA(e, n);

    return 0;
}

void readdata(emp *e1)
{
    printf("Enter employee details\n");
    printf("Enter the fist name: ");
    scanf("%s", e1->fname);
    printf("Enter the surname: ");
    scanf("%s", e1->lname);
    printf("Enter the current salary: ");
    scanf("%lf", &e1->salary);
    printf("Enter the current year: ");
    scanf("%d", &e1->year);
}

void disdata(emp *e1)
{
    printf("Employee name and salary details\n");
    printf("Employee name  Current year%d  next year%d\n", e1->year, e1->year + 1);
    printf("%-8s%-8s %0.2f%-15c %0.2f'%-20c'\n", e1->fname, e1->lname, e1->salary, '$', e1->salary * 1.1, '$');
}

void ACCEPT_DATA(emp *s, int count)
{
    for (int i = 0; i < count; i++)
    {
        readdata(&s[i]);
    }
}

void PRINT_DATA(emp *s, int count)
{
    for (int i = 0; i < count; i++)
    {
        disdata(&s[i]);
        printf("\n");
    }
}