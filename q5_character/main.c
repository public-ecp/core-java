#include <stdio.h>

/**
 * @brief Detects category of entered character
 * 
 * @return int 
 */
int main()
{
    char c;

    printf("Enter any character: ");
    scanf("%c", &c);
    if (c >= 'a' && c <= 'z')
        printf("LOWERCASE");
    else if (c >= 'A' && c <= 'Z')
        printf("UPPERCASE");
    else if (c >= '0' && c <= '9')
        printf("DIGIT");
    else if (c ==' ' || c =='\t' || c =='\n')
        printf("SPACE");
    else
        printf("OTHER");
    return 0;
}