#include<stdio.h>
void reverse_array(char* arr);
unsigned int my_strlen(char *str);
int main(){
 char str[20];
    printf("Enter string: ");
    scanf("%19s", str);
    reverse_array(str);
    printf("Reversed string: %s", str);

    return 0;
}


/**
 * @brief Reverses array
 *
 * @param arr pointer to array
 * @param size size of array
 */
void reverse_array(char* arr)
{
    unsigned int size = my_strlen(arr);
    for (unsigned int i = 0; i < size / 2; i++)
    {
        char tmp = *(arr + i);
        *(arr + i) = *(arr + size - i - 1);
        *(arr + size - 1 - i) = tmp;
    }
}

unsigned int my_strlen(char *str)
{
    unsigned short len;
    for (unsigned int i = 0; *(str + i) != '\0'; i++)
        len++;

    return len;
}