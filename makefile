target=main
libdir=../q1_lib

$(target).exe: $(target).o 
	gcc -o $(target).exe $(target).o 

$(target).o: $(target).c
	gcc -c $(target).c

run:
	./$(target).exe

clean:
	rm $(target).exe *.o

.phony: run clean
