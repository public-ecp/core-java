#include <stdio.h>
unsigned int my_strlen(char *str);
unsigned short isPalindrome(char *str);
int main()
{

    char str[20];
    printf("Enter string: ");
    scanf("%19s", str);
    if (isPalindrome(str) == 1)
        printf("%s is a Palindrome", str);
    else
        printf("%s is not a Palindrome", str);
    return 0;
}

unsigned short isPalindrome(char *str)
{
    unsigned int len = my_strlen(str);
    for (unsigned int i = 0; i < len / 2; i++)
        if (*(str + i) != *(str + len - 1 - i))
            return 0;
    return 1;
}

unsigned int my_strlen(char *str)
{
    unsigned short len;
    for (unsigned int i = 0; *(str + i) != '\0'; i++)
        len++;

    return len;
}