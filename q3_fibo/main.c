/* C program for Tabulated version */
#include <stdio.h>
#include <time.h>

/**
 * @brief Print fibonacci series till given number
 * 
 * @param n number
 * @return int fibonacci number
 */
int fib(int n)
{
    int f[n + 1];
    int i;

    f[0] = 0;
    f[1] = 1;
    for (i = 2;; i++)
    {
        f[i] = f[i - 1] + f[i - 2];
        if (i > n+1)
            break;
        printf("%d ", f[i]);
    }

    return f[n];
}

int main()
{
    int n;
    printf("Enter any number: ");
    scanf("%d", &n);

    fib(n);

    return 0;
}
