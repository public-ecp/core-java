#include<stdio.h>
#include<stdlib.h>
#include<limits.h>


int main(int argc, char *argv[]){

    if(argc < 2){
        printf("Please enter atleast 1 parameter\n");
        return -1;
    }

    int max = atoi(argv[1]);
    
    for (short i = 2; i < argc; i++)
    {
        int n = atoi(argv[i]);
        if( n > max)
        max = n;
    }

    printf("Max value is: %d", max);
    

    return 0;
}