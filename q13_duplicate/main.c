/*
Declare two Arrays of type String. Find the duplicate values of an 
array of string values.
*/

#include<stdio.h>
#include<string.h>
int main()
{
    printf("To find the duplicate string values between two array of string\n");

    char arr[4][20];
    for(int i=0;i<4;i++)
    {
        printf("Enter name%d = ",i+1);
        scanf("%s",arr[i]);
    }
    printf("Before sort\n");
    for(int i=0;i<4;i++)
        printf("%s\t",arr[i]);

    for(int i=0;i<4;i++)
    {
        for(int j=i+1;j<4;j++)
        {
            if(strcmp(arr[i],arr[j])>0)
            {
                char temp[20];
                strcpy(temp,arr[i]);
                strcpy(arr[i],arr[j]);
                strcpy(arr[j],temp);
            }
        }
    }

    printf("\nAfter sort\n");
    for(int i=0;i<4;i++)
        printf("%s\t",arr[i]);

    return 0;

}