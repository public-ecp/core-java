#include<stdio.h>
#include<math.h>

int convert(long long num) {
    int  bin = 0, rem = 0, place = 1;  
  
    while(num)  
    {  
        rem   = num % 2;  
        num   = num / 2;  
        bin   = bin + (rem * place);  
        place = place * 10;  
    } 
  return bin;
}


int main(){
    int n;

    printf("Enter any number: ");
    scanf("%d", &n);
    printf("Given number: %d\n", n);
    printf("Octal equivalent: %#o\n", n);
    printf("Hex equivalent: %#X\n", n);
    printf("Binary equivalent: %lld", convert(n));

}