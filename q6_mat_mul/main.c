#include <stdio.h>
#define ROWA 2
#define COLA 3
#define ROWB 3
#define COLB 1

// c = ROWA*COLB
int main()
{

    int a[][COLA] = {{1, 2, 3}, {4, 5, 6}};
    int b[][COLB] = {{1}, {4}, {7}};
    int c[ROWA][COLB] = {0};

    for (int i = 0; i < ROWA; i++)
    {
        for (int j = 0; j < COLB; j++)
        {
            c[i][j] = mat_mul(a, b, i, j);
            printf("%4d",c[i][j]);
        }
        printf("\n");
    }

    return 0;
}
/*
a => m*n
b => n*k
c => m*k
    1 2 3      1   => 30
    4 5 6      4   => 66
               7
    m*n         n*m
    ROW*COL     COL*ROW

*/

int mat_mul(int (*a)[COLA], int (*b)[COLB], int row, int col)
{
    int res = 0;
    for (int i = 0; i < COLA; i++)
    {
        // printf("Multiplying %d with %d => row=%d, col=%d, i=%d\n", *(*(a + row) + i),*(*(b + i) + col), row, col, i);
        res += *(*(a + row) + i) * *(*(b + i) + col);
    }
    return res;
}