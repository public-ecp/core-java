#include <stdio.h>

int main()
{

    char names[10][20];
    unsigned short n;

    printf("Number of students: ");
    scanf("%hu", &n);

    for (unsigned short i = 0; i < n; i++)
    {
        printf("Enter name %hu: ", i);
        scanf("%19s", names[i]);
    }

    printf("Names of students are as follows: \n");
    for (unsigned short i = 0; i < n; i++)
    {
        printf("%s\n", names[i]);
    }

    return 0;
}